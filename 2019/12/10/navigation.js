function getPageNumber() {
    let path = window.location.pathname;

    if (path.startsWith('/')) {
        path = path.substring(1);
    }

    path = path.substring(0, path.indexOf('.'));

    return parseInt(path);
}

function pageNumberToFilename(pageNum) {
    return pageNum + '.html';
}

function numberToPage(pageNum) {
    return ('0' + (pageNum)).slice(-2) + '.html';
}

function leftArrowPressed() {
    let pageNum = getPageNumber();

    if (pageNum === 0) {
        return;
    }

    let prevPage = numberToPage(getPageNumber() - 1);
    queuePage(prevPage);
}

function rightArrowPressed() {
    let nextPage = numberToPage(getPageNumber() + 1);
    queuePage(nextPage);
}

function queuePage(thePage) {
    let request = new XMLHttpRequest();

    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            console.log('Page ' + thePage + ' exists');
            window.location = thePage;
        } else if (this.readyState == 4 && this.status == 404) {
            console.log('Page ' + thePage + ' does not exist');
        }
    }

    request.open('GET', thePage, true);
    request.send();
}

document.onkeydown = function(event) {
    event = event || window.event;

    let rightCodes = [34, 39, 40];
    let leftCodes = [33, 37, 38];

    if (rightCodes.includes(event.keyCode)) {
        rightArrowPressed();
    } else if (leftCodes.includes(event.keyCode)) {
        leftArrowPressed();
    }
}
