#!/bin/bash

print_help() {
    echo "Usage:"
    echo "    $0 serve"
    echo "    $0 help"
    echo -e
}

serve() {
    live-server --port=10100 --no-browser .
    return $?
}

################################################################################
# Main block
################################################################################

if [ $# -lt 1 ]; then
    print_help
    exit 1
fi

case $1 in
serve)
    serve
    exit $?
    ;;
help)
    print_help
    exit $?
    ;;
*)
    print_help
    exit 1
    ;;
esac
