let misses = 0;
const GIVE_UP_MISS_COUNT = 10;

function getPageNumber() {
    let path = window.location.pathname;

    if (path.startsWith('/')) {
        path = path.substring(1);
    }

    path = path.substring(0, path.indexOf('.'));

    return parseInt(path);
}

function pageNumberToFilename(pageNum) {
    return pageNum + '.html';
}

function numberToPage(pageNum) {
    return ('0' + (pageNum)).slice(-2) + '.html';
}

function leftArrowPressed() {
    let pageNum = getPageNumber();

    if (pageNum === 0) {
        return;
    }

    queuePage(pageNum - 1, 'left');
}

function rightArrowPressed() {
    queuePage(getPageNumber() + 1, 'right');
}

function queuePage(pageNum, dir = 'right') {

    if (dir === 'right' && (misses > GIVE_UP_MISS_COUNT)) {
        console.log('Unable to find next page within 10 number gap. Giving up.');
        return;
    }

    let request = new XMLHttpRequest();

    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            console.log('Page ' + pageNum + ' exists');
            misses = 0;
            window.location = numberToPage(pageNum);

        } else if (this.readyState == 4 && this.status == 404) {
            misses++;
            console.log('Page ' + pageNum + ' does not exist. Misses: ' + misses);

            if (dir === 'right') {
                queuePage(pageNum + 1, 'right');
            } else {
                queuePage(pageNum - 1, 'left');
            }
        }
    }

    request.open('GET', numberToPage(pageNum), true);
    request.send();
}

document.onkeydown = function(event) {
    event = event || window.event;

    /*
     * Key code legend:
     *
     * 33 - page up
     * 34 - page down
     * 37 - arrow left
     * 38 - arrow up
     * 39 - arrow right
     * 40 - arrow down
     */

    let rightCodes = [39];
    let leftCodes = [37];

    if (rightCodes.includes(event.keyCode)) {
        rightArrowPressed();
    } else if (leftCodes.includes(event.keyCode)) {
        leftArrowPressed();
    }

    // console.log('keyCode: ' + event.keyCode);
}
