---
theme: seriph
background: images/4.jpg
class: 'text-center'
highlighter: shiki
lineNumbers: false
drawings:
  persist: false
---

# Ansible, by Red Hat

<br>

<div>The thing that runs stuff somewhere else</div>

<div class="abs-br m-6" style="color: grey;">
  by Nobby Knox
</div>

---

# Preparation

Do this before the presentation

```bash
sudo useradd -m -s /usr/bin/fish user1
sudo passwd user1
sudo usermod -a -G wheel user1
```
---

# What we will cover

* Part 1: The <span style="font-size: 200%;">🥱</span> bit
  - <span style="font-size: 200%;">🤨</span>

  - <span style="font-size: 200%;">🤷‍♂️</span>

  - <span style="font-size: 200%;">🤓</span>

* Part 2: The <span style="font-size: 200%;">🕺</span> bit

---
layout: image-right
image: https://d22e4d61ky6061.cloudfront.net/sites/default/files/ansible_0.png
---

# What is Ansible?

<br>

From their website:

<br>

_Red Hat Ansible Automation Platform is a foundation for building and operating automation across an organization._

<br>

Did you get that? 😕

---
layout: image-right
image: https://d22e4d61ky6061.cloudfront.net/sites/default/files/ansible_0.png
---

# Seriously, what is Ansible?

* Ansible is an open-source software
* It enables _infrastructure as code_:
  - Provisioning
  - Configuration management
  - Application deployment
* It runs everywhere: Linux, MacOS, and Windows
* Ansible is agentless, connecting remotely via SSH or Windows Remote Management (allowing remote PowerShell execution).

---
layout: image-right
image: https://d22e4d61ky6061.cloudfront.net/sites/default/files/ansible_0.png
---

# More about Ansible

What do you need to get started?

On your orchestration machine (your laptop, Jenkins worker node, etc):

* Python
* Ansible

On the target hosts:

* Apart from SSH? Nothing. 😁

---
layout: image-right
image: https://d22e4d61ky6061.cloudfront.net/sites/default/files/ansible_0.png
---

# Why do we need Ansible?

What problem does it solve?

With Ansible:

* You can automate tedious and repeatable tasks on a large number of target machines, with a single command
* The remote machines need no special agent software in order to work
* Your task definitions and inventory files can be versioned along with your code
* Tasks are idempotent* - run them again and again

<div class="abs-br m-6" style="color: grey;">
    Why Ansible? (1/2)
</div>

---
layout: image-right
image: https://d22e4d61ky6061.cloudfront.net/sites/default/files/ansible_0.png
---

# Why do we need Ansible?

<br>

But:

* Some overlap in functionality with other tools
* **Terraform** has stolen much of Ansible's thunder in terms of provisioning
* **Ansible** still king in other areas where it completes with tools like Puppet and Chef

<div class="abs-br m-6" style="color: grey;">
    Why Ansible? (2/2)
</div>

---
layout: image-right
image: https://d22e4d61ky6061.cloudfront.net/sites/default/files/ansible_0.png
---

# How does it work?

It sounds too good to be true, right?

QUESTION (retorical)

What can you do with SSH access to a host?

<br>

ANSWER

Well... almost everything

<!-- 💡 And there you have it. That is how Ansible works. Don't worry, we'll look into the detail some more. -->

---
layout: image-right
image: https://d22e4d61ky6061.cloudfront.net/sites/default/files/ansible_0.png
---

# More on the How

Looking closer...

```mermaid {theme: 'neutral', scale: 0.8}
flowchart TB
    subgraph laptop
    inv(inventory) --host information--> ans(ansible)
    book(playbook) --task instructions--> ans(ansible)
    end
    subgraph remote
    ans --tasks--> host1(host 1)
    ans --tasks--> host2(host 2)
    ans --tasks--> host3(host 3)
    ans --tasks--> hostx(host n)
    end
```

---

# An Inventory File - YAML

An inventory of hosts to operate on

```yaml
all:
  hosts:
    mail.example.com:
  children:
    webservers:
      hosts:
        foo.example.com:
        bar.example.com:
    dbservers:
      hosts:
        one.example.com:
        two.example.com:
        three.example.com:
```

[Learn more](https://docs.ansible.com/ansible-core/devel/user_guide/intro_inventory.html#intro-inventory)

---

# Another Inventory File - INI

```ini
mail.example.com

[webservers]
foo.example.com
bar.example.com

[dbservers]
one.example.com
two.example.com
three.example.com
```

[Learn more](https://docs.ansible.com/ansible-core/devel/user_guide/intro_inventory.html#intro-inventory)

---

# Ansible @ VSS

So, how do _we_ use it?

🔒 / 🔓 secrets files

<div class="abs-br m-6" style="color: grey;">
    Ansible @ VSS (1/4)
</div>

---

# Ansible @ VSS

From **values-test-secrets.yaml**

<div style="display: flex; justify-content: center;">
    <img src="images/code-secrets-file.png" class="rounded shadow" style="width: 80%;" />
</div>

<div class="abs-br m-6" style="color: grey;">
    Ansible @ VSS (2/4)
</div>

---

# Ansible @ VSS

From **build.dockerfile**:

<div style="display: flex; justify-content: center;">
    <img src="images/code-dockerfile.png" class="rounded shadow" style="width: 60%;" />
</div>

<div class="abs-br m-6" style="color: grey;">
    Ansible @ VSS (3/4)
</div>

---

# Ansible @ VSS

From **Jenkinsfile.TEST-CLOUD.bitbucket**:

<div style="display: flex; justify-content: center;">
    <img src="images/code-jenkins.png" class="rounded shadow" style="width: 80%;" />
</div>

<div class="abs-br m-6" style="color: grey;">
    Ansible @ VSS (4/4)
</div>

---
layout: cover
background: images/3.jpg
---

# Now for the exciting bit...

a demo

---
layout: image-right
image: images/3.jpg
---

# We'll see

* Provisioning of sorts... maybe not so much
* Installation of stuff
* Configuration of things
* Not the old boring server stuff
<br>

<span style="font-size: 200%;">🤔</span>

---
layout: cover
background: https://cloudberrytec.com/wp-content/uploads/2022/01/fotis-fotopoulos-LJ9KY8pIH3E-unsplash-1024x683.jpg
---

We're going to ...

---
layout: cover
background: https://cloudberrytec.com/wp-content/uploads/2022/01/fotis-fotopoulos-LJ9KY8pIH3E-unsplash-1024x683.jpg
---

"Image" a general development environment

---
layout: cover
background: https://cloudberrytec.com/wp-content/uploads/2022/01/fotis-fotopoulos-LJ9KY8pIH3E-unsplash-1024x683.jpg

---

On a laptop!

---
layout: image-right
image: https://cloudberrytec.com/wp-content/uploads/2022/01/fotis-fotopoulos-LJ9KY8pIH3E-unsplash-1024x683.jpg
---

# But Why?

The idea is not that far-fetched.

* Setting up a dev machine is tedious
* and consists of the same repeatable tasks

Remember that Ansible likes repeatable tasks!

---
layout: image-right
image: https://cloudberrytec.com/wp-content/uploads/2022/01/fotis-fotopoulos-LJ9KY8pIH3E-unsplash-1024x683.jpg
---

# What to Expect?

1. Get our hands dirty with a "Hello World!" example
1. Provision the development environment
1. Overview of the project

---

# Hello World

* Run the _hello world_ playbook

```yaml
- name: Hello World example

  vars:
    greeting_file: /tmp/greeting

  tasks:
    - name: Create a file called 'greeting' with the content 'Hello World!'.
      copy:
        content: "Hello World!"
        dest: "{{ greeting_file }}"
```

---
layout: image-right
image: https://cloudberrytec.com/wp-content/uploads/2022/01/fotis-fotopoulos-LJ9KY8pIH3E-unsplash-1024x683.jpg
---

# Main Playbook

* Run the main playbook
* It will take a few minutes

---
layout: image-right
image: https://cloudberrytec.com/wp-content/uploads/2022/01/fotis-fotopoulos-LJ9KY8pIH3E-unsplash-1024x683.jpg
---

# Showcase

* Fonts! Fonts!
* Configure the _Tide_ prompt
* Visual Studio Code, extensions and settings
* SDKMAN (software development kit manager)
    - install java
    - install maven
* NVM (Node.js version manager)
    - install node.js
* Jetbrains Toolbox
    - install IntelliJ IDEA
* Git customization
* Sample project cloned and ready to hack

---
layout: cover
background: http://www.velosdesignwerks.com/wp-content/uploads/2015/01/f10-m5-diffuser3.jpg
---

# Thanks for coming

🖖
