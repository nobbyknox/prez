let slideIndex = 0;
let slides = 0;
let showAll = false;

document.addEventListener('DOMContentLoaded', function (event) {
    init();
})

function init() {
    slides = document.getElementsByTagName('section');
    console.log(`${slides.length} slides found`);

    if (slides.length > 0) {
        for (let i = 0; i < slides.length; i++) {
            slides[i].classList.add('hidden');
        }

        slides[0].classList.remove('hidden');
    }

    let lines = document.getElementsByTagName('hr');

    if (lines.length > 0) {
        for (let i = 0; i < lines.length; i++) {
            lines[i].classList.add('hidden');
        }
    }
}

function leftArrowPressed() {
    if (slideIndex <= 0)
        return;

    slideIndex--;
    redraw();
}

function rightArrowPressed() {
    if (slideIndex >= slides.length - 1)
        return;

    slideIndex++;
    redraw();
}

function redraw() {
    if (slides.length == 0)
        return;

    for (let i = 0; i < slides.length; i++) {
        slides[i].classList.add('hidden');
    }

    if (slideIndex < slides.length) {
        slides[slideIndex].classList.remove('hidden');
    }
}

function toggleAllSlides() {
    showAll = !showAll;
    
    showOrHide(document.getElementsByTagName('section'), showAll);
    showOrHide(document.getElementsByTagName('hr'), showAll);
    showOrHide(document.getElementsByClassName('at-bottom-left'), !showAll);
    showOrHide(document.getElementsByClassName('at-bottom-right'), !showAll);
    slides[slideIndex].classList.remove('hidden');
}

function showOrHide(elements, show) {
    if (elements && elements.length > 0) {
        for (let i = 0; i < elements.length; i++) {
            if (show) {
                elements[i].classList.remove('hidden');
            } else {
                elements[i].classList.add('hidden');
            }
        }
    }
}

document.onkeydown = function(event) {
    event = event || window.event;

    /*
     * Key code legend:
     *
     * 33 - page up
     * 34 - page down
     * 37 - arrow left
     * 38 - arrow up
     * 39 - arrow right
     * 40 - arrow down
     * 65 - a
     */

    let rightCodes = [39];
    let leftCodes = [37];
    let printCodes = [65];

    if (rightCodes.includes(event.keyCode)) {
        rightArrowPressed();
    } else if (leftCodes.includes(event.keyCode)) {
        leftArrowPressed();
    } else if (printCodes.includes(event.keyCode)) {
        toggleAllSlides();
    }

    // console.log('keyCode: ' + event.keyCode);
}
